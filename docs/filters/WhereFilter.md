This filters allows you to filter using the query ->where function

You can set the comparator as shown below

```
<?php

use NicoSorice\QueryFilterer\PackageClasses\QueryFilterer;
use NicoSorice\QueryFilterer\PackageClasses\Filters\WhereFilter;

New QueryFilterer(
    Model::query(),
    request()
 )->filter([
     'field'                     => WhereFilter::class,
     'another_field'             => new WhereFilter('>'),
     'just_another_field'        => (new WhereFilter)->setComparator('>'),
 ])
```