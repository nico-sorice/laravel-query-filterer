<?php

namespace NicoSorice\QueryFilterer\Interfaces;

/**
 * Interface QueryFilterContract
 * @package NicoSorice\QueryFilterer\Interfaces
 */
interface QueryFilterContract
{
    public function filter($query, $requestValue, string $filterKey): void;
}