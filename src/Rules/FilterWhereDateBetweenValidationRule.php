<?php

namespace NicoSorice\QueryFilterer\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use NicoSorice\QueryFilterer\PackageClasses\Filters\WhereDateBetweenFilter;

/**
 * Class FilterWhereDateBetweenValidationRule
 * @package NicoSorice\Rules
 */
class FilterWhereDateBetweenValidationRule implements Rule
{
    /**
     * @var string
     */
    protected $format = 'y-m-d';

    /**
     * @var string
     */
    protected $minKey = 'min';

    /**
     * @var string
     */
    protected $maxKey = 'max';

    /**
     * FilterWhereDateBetweenRule constructor.
     */
    public function __construct()
    {
        if(WhereDateBetweenFilter::$defaultFormat)
        {
            $this->format = WhereDateBetweenFilter::$defaultFormat;
        }
    }

    /**
     * @param string $attribute
     * @param mixed $value
     * @return mixed
     */
    public function passes($attribute, $value)
    {
        $validator = Validator::make($value, [
            $this->minKey => "nullable|date_format:$this->format",
            $this->maxKey => "nullable|date_format:$this->format",
        ]);

        return $validator->passes();
    }

    /**
     * @return mixed
     */
    public function message()
    {
        $key = 'test'; //TODO: TRANSLATIONS

        return trans('queryfilterer::validation.filter_where_date_between', ['attribute' => trans("validation.attributes.{$key}")]);
    }
}