<?php

namespace NicoSorice\QueryFilterer\PackageClasses;

use Illuminate\Http\Request;
use NicoSorice\QueryFilterer\Exceptions\QueryFilterConfigurationException;
use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class QueryFilterer
 * @package NicoSorice\QueryFilterer\PackageClasses
 */
class QueryFilterer
{
    /**
     * @var
     */
    protected $query;

    /**
     * @var Request
     */
    protected $request;

    /**
     * QueryFilterer constructor.
     * @param $query
     * @param Request $request
     */
    public function __construct($query, Request $request)
    {
        $this->query = $query;
        $this->request = $request;
    }

    /**
     * @param array $config
     * @return self
     * @throws QueryFilterConfigurationException
     */
    public function filter(array $config = []): self
    {
        foreach($config as $requestKey => $filter)
        {
            if(is_array($filter))
            {
                $filterKey = array_keys($filter)[0];
                $filterClass = array_values($filter)[0];
            }
            else
            {
                if(is_numeric($requestKey))
                {
                    $filterKey = null;
                }
                else
                {
                    $filterKey = $requestKey;
                }

                $filterClass = $filter;
            }

            if($filterClass instanceof QueryFilterContract)
            {
                $filterInstance = $filter;
            }
            elseif(is_a($filterClass, QueryFilterContract::class, true))
            {
                $filterInstance = new $filterClass();
            }
            else
            {
                throw new QueryFilterConfigurationException(/*$filter*/);
            }

            $filterInstance->filter(
                $this->query,
                ($filterKey === null) ? $this->request->input() : $this->request->get($requestKey),
                $filterKey
            );
        }

        return $this;
    }
}