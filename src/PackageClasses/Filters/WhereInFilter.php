<?php

namespace NicoSorice\QueryFilterer\PackageClasses\Filters;

use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class WhereInFilter
 * @package NicoSorice\QueryFilterer\PackageClasses\Filters
 */
class WhereInFilter implements QueryFilterContract
{
    /**
     * @param $query
     * @param $requestValue
     * @param string $filterKey
     */
    public function filter($query, $requestValue, string $filterKey): void
    {
        if(isset($requestValue) && $requestValue !== '')
        {
            $query->whereIn($filterKey, (array) $requestValue);
        }
    }
}