<?php

namespace NicoSorice\QueryFilterer\PackageClasses\Filters;

use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class WhereDateBetweenFilter
 * @package NicoSorice\QueryFilterer\PackageClasses\Filters
 */
class WhereDateBetweenFilter implements QueryFilterContract
{
    /**
     * @var string
     */
    public static $defaultFormat;

    /**
     * @var string
     */
    protected $format = 'y-m-d';

    /**
     * @var string
     */
    protected $minKey = 'min';

    /**
     * @var string
     */
    protected $maxKey = 'max';

    /**
     * WhereDateBetween constructor.
     */
    public function __construct()
    {
        if(self::$defaultFormat)
        {
            $this->format = self::$defaultFormat;
        }
    }

    /**
     * @param string $format
     * @return self
     */
    public function setFormat(string $format): self
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @param string $minKey
     * @param string $maxKey
     * @return self
     */
    public function setKeys(string $minKey, string $maxKey): self
    {
        $this->minKey = $minKey;
        $this->maxKey = $maxKey;
        return $this;
    }

    /**
     * @param $query
     * @param $requestValue
     * @param string $filterKey
     */
    public function filter($query, $requestValue, string $filterKey): void
    {
        if(!empty($requestValue[$this->minKey]))
        {
            $query->whereDate($filterKey, '>=', \DateTime::createFromFormat($this->format, $requestValue[$this->minKey]));
        }

        if(!empty($requestValue[$this->maxKey]))
        {
            $query->whereDate($filterKey, '<=', \DateTime::createFromFormat($this->format, $requestValue[$this->maxKey]));
        }
    }
}