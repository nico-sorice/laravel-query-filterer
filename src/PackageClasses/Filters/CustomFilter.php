<?php

namespace NicoSorice\QueryFilterer\PackageClasses\Filters;

use NicoSorice\QueryFilterer\Exceptions\QueryFilterConfigurationException;
use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class CustomFilter
 * @package NicoSorice\QueryFilterer\PackageClasses\Filters
 */
class CustomFilter implements QueryFilterContract
{
    /**
     * @var string
     */
    protected $closure;

    /**
     * CustomFilter constructor.
     * @param \Closure|null $closure
     */
    public function __construct(\Closure $closure = null)
    {
        if($closure){
            $this->setClosure($closure);
        }
    }

    /**
     * @param \Closure $closure
     * @return CustomFilter
     */
    public function setClosure(\Closure $closure): self
    {
        $this->closure = $closure;
        return $this;
    }

    /**
     * @param $query
     * @param $requestValue
     * @param string $filterKey
     * @throws QueryFilterConfigurationException
     */
    public function filter($query, $requestValue, string $filterKey): void
    {
        $closure = $this->closure;

        if($this->closure === null){
            throw new QueryFilterConfigurationException('You have to specify the custom filter closure either passing it in the construtor or using ::setClosure method.');
        }

        $closure($query, $requestValue, $filterKey);
    }
}