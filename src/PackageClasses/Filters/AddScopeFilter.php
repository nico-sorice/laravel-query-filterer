<?php

namespace NicoSorice\QueryFilterer\PackageClasses\Filters;

use NicoSorice\QueryFilterer\Exceptions\QueryFilterConfigurationException;
use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class AddScopeFilter
 * @package NicoSorice\QueryFilterer\PackageClasses\Filters
 */
class AddScopeFilter implements QueryFilterContract
{
    /**
     * @var string
     */
    protected $scopeName;

    /**
     * @var array|null
     */
    protected $scopeParams;

    /**
     * AddScopeFilter constructor.
     * @param string|null $scopeName
     */
    public function __construct(string $scopeName = null, array $scopeParams = null)
    {
        $this->setScopeName($scopeName);
        $this->setScopeParams($scopeParams);
    }

    /**
     * @param string $scopeName
     * @return AddScopeFilter
     */
    public function setScopeName(string $scopeName): self
    {
        $this->scopeName = $scopeName;
        return $this;
    }

    /**
     * @param array $scopeParams
     * @return AddScopeFilter
     */
    public function setScopeParams(array $scopeParams): self
    {
        $this->scopeParams = $scopeParams;
        return $this;
    }

    /**
     * @param $query
     * @param $requestValue
     * @param string $filterKey
     * @throws QueryFilterConfigurationException
     */
    public function filter($query, $requestValue, string $filterKey): void
    {
        if(empty($this->scopeName))
        {
            throw new QueryFilterConfigurationException('ScopeName not set');
        }

        if($requestValue !== '')
        {
            if($this->scopeParams)
            {
                call_user_func_array([$query, $this->scopeName], $this->scopeParams);
            }
            else
            {
                call_user_func([$query, $this->scopeName]);
            }
        }
    }
}