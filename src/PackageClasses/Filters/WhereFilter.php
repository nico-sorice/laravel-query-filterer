<?php

namespace NicoSorice\QueryFilterer\PackageClasses\Filters;

use NicoSorice\QueryFilterer\Interfaces\QueryFilterContract;

/**
 * Class WhereFilter
 * @package NicoSorice\QueryFilterer\PackageClasses\Filters
 */
class WhereFilter implements QueryFilterContract
{
    /**
     * @var string
     */
    protected $comparator;

    /**
     * WhereFilter constructor.
     * @param string $comparator
     */
    public function __construct(string $comparator = '=')
    {
        $this->setComparator($comparator);
    }

    /**
     * @param string $comparator
     * @return self
     */
    public function setComparator(string $comparator): self
    {
        $this->comparator = $comparator;
        return $this;
    }

    /**
     * @param $query
     * @param $requestValue
     * @param string $filterKey
     */
    public function filter($query, $requestValue, string $filterKey): void
    {
        if(isset($requestValue) && $requestValue !== '')
        {
            $query->where($filterKey, $this->comparator, (array) $requestValue);
        }
    }
}