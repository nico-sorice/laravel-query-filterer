<?php

namespace NicoSorice\QueryFilterer;

use Illuminate\Support\ServiceProvider;

/**
 * Class QueryFiltererServiceProvider
 * @package NicoSorice\FacturaElectronica
 */
class QueryFiltererServiceProvider extends ServiceProvider
{
    /**
     *
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '../resources/lang', 'queryfilterer');

        $this->publishes([
            __DIR__ . '../resources/lang' => resource_path('lang/vendor/queryfilterer'),
        ], 'lang');
    }

    /**
     *
     */
    public function register()
    {

    }
}