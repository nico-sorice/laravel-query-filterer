<?php

namespace NicoSorice\QueryFilterer\Exceptions;

/**
 * Class QueryFilterConfigurationException
 * @package NicoSorice\QueryFilterer\Exceptions
 */
class QueryFilterConfigurationException extends \Exception
{

}