# Laravel Query Filterer

This package allows you to set conditional rules to filter a query according to the HTTP request values with ease and speed.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This package requires php7.0 or up, laravel 5.5 or up and composer installed on your computer.

```
composer
php >= 7.0.0
laravel >= 5.5
```

### Installation

Require using composer

```
composer require nico-sorice/laravel-query-filterer
```


### Usage

In your controller

```
<?php

use NicoSorice\QueryFilterer\PackageClasses\QueryFilterer;

New QueryFilterer(
    Model::query(),
    request()
 )->filter([
     'id'        => WhereFilter::class,
     'status_id' => WhereInFilter::class,
     'date'      => (new WhereDateBetweenFiter)->setFormat('d/m/Y'),
 ])
```

Full list of [available filters](docs/filters)


The package includes validation rules for the filters that require them

In your request

```
<?php

 /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'   => 'nullable|integer',
            'date' => [
                'nullable', 
                new FilterWhereDateBetweenValidationRule
            ]
        ];
    }
```

Full list of [available validation rules](docs/validation_rules)

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/nico-sorice/laravel-query-filterer/tags). 

## Authors

* **Nicolas Sorice** - *Initial work* - [GitLab Profile](https://gitlab.com/nico-sorice)

See also the list of [CONTRIBUTORS.md](CONTRIBUTORS.md) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
